import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';
import * as mongoose from 'mongoose';
export type WorkspaceDocument = Workspace & Document;

@Schema()
export class Workspace  {

  @Prop({ required: true })
  code: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  namespace: string;

  @Prop({ required: true })
  customer: string;

  @Prop({ required: true })
  icon: string;

  @Prop({ required: true })
  isTest: boolean;

  @Prop({ required: true })
  note: string;

  @Prop({ required:true, type: mongoose.Schema.Types.ObjectId})
  statusId: string;
}

export const WorkspaceSchema = SchemaFactory.createForClass(Workspace);
