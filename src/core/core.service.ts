import { HttpService } from "@nestjs/axios";
import { ForbiddenException, HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { CoreBodyDto } from "src/dto/coreBody.dto";
import { CoreQueryDto } from "src/dto/coreQuery.dto";
import * as _ from 'lodash'
@Injectable()
export class CoreService {
  constructor(private httpService: HttpService) {}
  getList(coreQuery : CoreQueryDto) : any {
    return this.httpService.get('/coreapi', { params: coreQuery }).toPromise().then(res => res.data).catch(this.handleError)
  }
  save(coreBody: CoreBodyDto) : any {
    return this.httpService.post('/coreapi', coreBody).toPromise().then(res => res.data).catch(this.handleError)
  }
  validate(coreBody: CoreBodyDto) : any {
    return this.httpService.post('/coreapi/validate', coreBody).toPromise().then(res => res.data).catch(this.handleError)
  }
  delete(coreBody: CoreBodyDto) : any {
    return this.httpService.delete('/coreapi', { data: coreBody }).toPromise().then(res => res.data).catch(this.handleError)
  }
  getCoreSchema(name: string, id: string) : any {
    return this.httpService.get('/coreapi/coreSchema', { params: { name, id } }).toPromise().then(res => res.data).catch(this.handleError)
  }
  getCoreSearchTypes(dataTypeId: string) : any {
    return this.httpService.get(`/coreapi/searchTypes/${dataTypeId}`).toPromise().then(res => res.data).catch(this.handleError)
  }
  downloadFileLog(mapId: string) : any {
    return this.httpService.get(`/coreapi/file/log/${mapId}`, { responseType: 'stream' }).toPromise()
  }
  downloadFileLogMimeType(mapId: string) : any {
    return this.httpService.get(`/coreapi/file/log/mimetype/${mapId}`).toPromise().then(res => res.data).catch(this.handleError)
  }
  downloadFile(fileId: string) : any {
    return this.httpService.get(`/coreapi/files/${fileId}`, { responseType: 'blob' }).toPromise().then(res => res.data).catch(this.handleError)
  }
  deleteFile(body: CoreBodyDto) : any {
    return this.httpService.delete('/coreapi/file', { data: body }).toPromise().then(res => res.data).catch(this.handleError)
  }
  handleError(error) {
    if (_.has(error, 'response.status') && _.has(error, 'response.data')) {
      const status = _.get(error, 'response.status')
      const data = _.get(error, 'response.data')
      throw new HttpException(data, status)
    } else if (error.response) {
      const data = error.response.data
      const message = data.message
      const status : HttpStatus = data.statusCode
      throw new HttpException(message, status)
    } else {
      throw new ForbiddenException(error)
    }
  }
  getCheckMail(email: string) : Promise<any> {
    return this.httpService.get(`/coreapi/check/userMail/${email}`).toPromise().then(res => res.data).catch(this.handleError)
  }
  getResetPassword(userId: string) : Promise<any> {
    return this.httpService.get(`/coreapi/reset/password/${userId}`).toPromise().then(res => res.data).catch(this.handleError)
  }
  checkConfirmCode(userId: string, confirmCode: string) : Promise<any> {
    return this.httpService.get('/coreapi/check/confirmCode', { params: { userId, confirmCode }}).toPromise().then(res => res.data).catch(this.handleError)
  }
}
