import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { CoreController } from "./core.controller";
import { CoreService } from "./core.service";

@Module({
  imports: [
    HttpModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        timeout: 5000,
        maxRedirects: 5,
        baseURL: configService.get('CORE_URL'),
        headers: { 'workspace': configService.get('WORKSPACE') }
      }),
      inject: [ConfigService],
    })
  ],
  providers: [CoreService],
  controllers: [CoreController],
  exports: [CoreService]
})
export class CoreModule {}
