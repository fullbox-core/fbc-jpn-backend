import { UseGuards, Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Query, Req, Res, UseInterceptors } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { CoreService } from './core.service';
import { CoreBodyDto } from '../dto/coreBody.dto';
import { CoreQueryDto } from '../dto/coreQuery.dto';
@UseGuards(JwtAuthGuard)
@Controller('core')
export class CoreController {
  constructor(private readonly service: CoreService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async getList(@Req() req :any , @Query() coreQuery: CoreQueryDto) : Promise<any> {
    console.log('req', req.user)
    if(coreQuery.id && coreQuery.id.toString() === '623d7ebf366b29cb2e3af8c2') {// Гэрээ
      if (req.user && req.user.roleId.toString() !== '62134541c28802055e538dfa') {
        coreQuery.search = [{ name: 'createdUser', value: req.user._id}]
      }
    }
    if (coreQuery.id && coreQuery.id.toString() === '6209dc07ad55f12638a2baff') {//Ажлын байр
      if (req.user && req.user.roleId.toString() !== '62134541c28802055e538dfa') { // Эрх
        coreQuery.search = [{ name: 'createdUser', value: req.user._id}]
      }
    }
    return this.service.getList(coreQuery)
  }
  // async getList(@Req() req, @Query() coreQuery: CoreQueryDto) : Promise<any> {
  //   var search: any = null
  //   if (coreQuery.name) {
  //     search = {
  //       name: 'schemaName',
  //       value: coreQuery.name
  //     }
  //   } else {
  //     search = {
  //       name: 'schemaId',
  //       value: coreQuery.id
  //     }
  //   }
  //   if (coreQuery.id && coreQuery.id.toString() === '623d7ebf366b29cb2e3af8c2') {
  //     coreQuery.search = [{ name: 'createdUser', value: req.user._id}]
  //   }
  //   if (coreQuery.id && coreQuery.id.toString() === '6209dc07ad55f12638a2baff') {
  //     if (req.user && req.user.roleId.toString() !== '62134541c28802055e538dfa') {
  //       coreQuery.search = [{ name: 'createdUser', value: req.user._id}]
  //     }

  //   }
  //   const schemaFilters = (await this.service.getList({ name: 'coreJwtFilter', search: [ search ] })).rows
  //   if (schemaFilters && schemaFilters.length > 0) {
  //     const filters : any = []
  //     schemaFilters.map( item => {
  //       if(item.jwt) {
  //         if(req.user) {
  //           const filter = {
  //             name: item.keyName,
  //             value: req.user[item.jwt]
  //           }
  //           filters.push(filter)
  //         }
  //       }
  //     })
  //     if (filters && filters.length > 0) {
  //       // if (Array.isArray(coreQuery.search) || coreQuery.search !== undefined) {
  //       if (Array.isArray(coreQuery.search)) {
  //         coreQuery.search.concat(filters)
  //       } else {
  //         coreQuery.search = filters
  //       }
  //     }
  //   }
  //   return await this.service.getList(coreQuery)
  // }
  @Post('validate')
  validate(@Body() body: CoreBodyDto) : any {
    return this.service.validate(body)
  }
  @Post()
  save(@Req() req: any, @Body() body: CoreBodyDto) : any {
    if (body.id === '6209dc07ad55f12638a2baff' || '623d7ebf366b29cb2e3af8c2') {
      body.data.createdUser = req.user._id
    }
    return this.service.save(body)
  }
  @Delete()
  delete(@Body() body: CoreBodyDto) : any {
    return this.service.delete(body)
  }
  @Get('coreSchema')
  getCoreSchema(@Query('name') name: string, @Query('id') id: string) : any {
    return this.service.getCoreSchema(name, id)
  }
  @Get('searchTypes/:dataTypeId')
  getCoreSearchTypes(@Param('dataTypeId') dataTypeId : string) : any {
    return this.service.getCoreSearchTypes(dataTypeId)
  }
  @Get('file/log/mimetype/:mapId')
  downloadFileLogMimeType(@Param('mapId') mapId : string) {
    return this.service.downloadFileLogMimeType(mapId)
  }
  @Get('files/:fileName')
  downloadFile(@Param('fileName') fileName : string) {
    return this.service.downloadFile(fileName)
  }
  @Delete('file')
  deleteFile(@Body() file: any) {
    return this.service.deleteFile(file)
  }
  @Get('file/log/:mapId')
  async downloadFileLog(@Res() res, @Param('mapId') mapId : string) : Promise<any> {
    try {
      const response = await this.service.downloadFileLog(mapId)
      response.data.pipe(res)
    } catch (error) {
      // console.log('error', error)
      const data = error.response.data
      const message = data.message
      const status : HttpStatus = data.statusCode
      if (status === HttpStatus.NOT_FOUND) {
        throw new HttpException('Файл олдсонгүй.', status)
      } else {
        throw new HttpException(message, status)
      }
    }
  }
  @Get('checkMail/:email')
  async getCheckMail(@Param('email') email : string) : Promise<any> {
    return this.service.getCheckMail(email)
  }
  @Get('resetPassword/:userId')
  async getResetPassword(@Param('userId') userId : string) : Promise<any> {
    return this.service.getResetPassword(userId)
  }
  @Get('confirmCode')
  async checkConfirmCode(@Query('userId') userId: string, @Query('confirmCode') confirmCode: string) : Promise<any> {
    console.log(userId, confirmCode, '----password----')
    return this.service.checkConfirmCode(userId, confirmCode)
  }
}
