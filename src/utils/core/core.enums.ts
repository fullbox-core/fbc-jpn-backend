export enum SEARCHTYPE {
  EQUALS = "equals",
  NOT_EQUALS = "notEquals",
  CONTAINS = "contain",
  NOT_CONTAINS = "notContain",
  STARTS = "starts",
  ENDS = "ends",
  GREATER = "great",
  LESS = "less",
  BETWEEN = "between",
  IS_NULL = "isNull",
  IS_NOT_NULL = "isNotNull"
}

export enum DATATYPE {
  Boolean = "Boolean",
  Number = "Number",
  Image = "Image",
  File = "File",
  Date = "Date",
  String = "String",
  Schema = "Schema",
  DataType = "DataType",
  Password = "Password",
  Datetime = "Datetime"
}

export enum DATETYPE {
  Date,
  End
}