const bcrypt = require('bcrypt');
const saltRounds = 10;

export class Password {
    static async getHash(password: string) {
        const salt = await bcrypt.genSalt(saltRounds);
        const hash = await bcrypt.hash(password, salt)
        return hash
    }
    static async compare(password: string, hash: string){
        return await bcrypt.compare(password, hash)
    }
}