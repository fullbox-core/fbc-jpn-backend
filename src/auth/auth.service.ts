
import { ConsoleLogger, ForbiddenException, HttpException, Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';

import { JwtService } from '@nestjs/jwt';
import { Types, Connection } from 'mongoose';
import { Password } from '../utils/Password'
import * as _ from 'lodash'
import { CoreService } from 'src/core/core.service';
const axios = require('axios')
@Injectable()
export class AuthService {
  constructor(
    @InjectConnection() private readonly connection: Connection,
    private jwtService: JwtService,
    private coreService: CoreService
  ) {
  }
  async validateUser(userId: string, type: string): Promise<any> {
    let user = null
    user = await this.connection.collection('baseUser').findOne({_id: new Types.ObjectId(userId), isActive: true, deleted: false})
    if (!user) throw new UnauthorizedException()
    return user;
  }
  async login(loginModel: any) {
    const { name, password } = loginModel
    console.log(name, password, '---1---')
    const user = await this.connection.collection('baseUser').findOne({
      name: name,
      deleted: false,
      isActive: true
    })
    if (!user) {
      throw "user not found"
    }
    console.log(user, '---login---')
    const isMatch = await Password.compare(password, user.passwordHash)
    if (!isMatch) {
      throw "User password is not match!"
    }
    const payload = { username: user.name, userId: user._id };
    try {
      return this.jwtService.sign(payload, { expiresIn: '1800s' })
    } catch (error) {
      throw 'Error on sign token - ' + error
    }
  }
}
