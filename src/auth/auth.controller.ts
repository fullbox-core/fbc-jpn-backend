import { ForbiddenException, Controller, Get, Post,Res, UseGuards, Req, Body, HttpStatus, Param } from '@nestjs/common';
import { AuthService } from './auth.service';
import { CoreService } from 'src/core/core.service';
import { Response } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import * as _ from 'lodash'

interface LoginModel {
  name: string,
  password: string
}
@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private coreService: CoreService
  ) {}

  @Post('login')
  async  login(@Res() res: Response, @Body() loginModel: LoginModel) {
  try {
    console.log(loginModel, '---worked---')
    const result = await this.authService.login(loginModel)
    res.status(HttpStatus.OK).send(result);
    } catch(err) {
      res.status(HttpStatus.FORBIDDEN).send({message: err});
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('userInfo')
  async getUserInfo(@Req() req) {
    return req.user
  }

  //#region Admin
  @UseGuards(JwtAuthGuard)
  @Get('menus')
  async getMenus(@Req() req) : Promise<any> {
    if (!req.user.roleId) {
      throw new ForbiddenException('Хандах эрх тохируулаагүй байна.')
    }
    const { roleId, workspace } = req.user
    const role = await this.coreService.getList({
      name: 'baseUserRole', single: true,
      search: [ { name: '_id', value: roleId }, { name: 'workspace', value: workspace } ]
    })
    if (!role) {
      throw new ForbiddenException('Хандах эрх тохируулаагүй байна.')
    }
    const rolemenuactions = await this.coreService.getList({
      name: 'baseUserRoleMenuActions',
      search: [ { name: 'roleId', value: role._id }, { name: 'workspace', value: workspace } ]
    })
    if (!rolemenuactions && (rolemenuactions && !rolemenuactions.rows)) {
      throw new ForbiddenException('Хандах эрх тохируулаагүй байна.')
    }
    const menus = []
    for (const r of rolemenuactions.rows) {
      menus.push(r.workspaceMenu)
    }
    return menus
  }

  @UseGuards(JwtAuthGuard)
  @Get('actions')
  async getActions(@Req() req) : Promise<any> {
    const { roleId } = req.user
    console.log(req.user, '----------user0000000--------')
    const role = await this.coreService.getList({
      name: 'baseUserRole', single: true,
      search: [ { name: '_id', value: roleId } ]
    })
    if (!role) {
      throw new ForbiddenException('Хандах эрх тохируулаагүй байна.')
    }
    const rolemenuactions = await this.coreService.getList({
      name: 'baseUserRoleMenuActions',
      search: [ { name: 'roleId', value: role._id } ]
    })
    if (!rolemenuactions && (rolemenuactions && !rolemenuactions.rows)) {
      throw new ForbiddenException('Хандах эрх тохируулаагүй байна.')
    }
    const search = []
    for (const r of rolemenuactions.rows) {
      for (const a of r.userRoleActionId) {
        search.push({ name: '_id', value: a })
      }
    }
    const roleactions = await this.coreService.getList({
      name: 'baseUserRoleActions', skipTotal: true,
      search: search
    })
    const sortedList = _.orderBy(roleactions, ['sortOrder'], ['asc'])
    return sortedList
  }
  //#endregion
}
