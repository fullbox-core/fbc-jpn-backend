import { Module, HttpModule } from '@nestjs/common';
import { MenuController } from './menu.controller';
import { MenuService } from './menu.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CoreModule } from 'src/core/core.module';
import { Workspace, WorkspaceSchema } from 'src/schemas/workspace/workspace.schema';

@Module({
  imports: [
    CoreModule,
    HttpModule,
    MongooseModule.forFeature([
      { name: Workspace.name, schema: WorkspaceSchema },
    ])
  ],
  controllers: [MenuController],
  providers: [MenuService]
})
export class MenuModule {}
