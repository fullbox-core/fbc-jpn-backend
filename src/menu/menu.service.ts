import { ForbiddenException, Req } from '@nestjs/common';
import { CoreService } from 'src/core/core.service';
import * as _ from 'lodash'
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';

export class MenuService {
  constructor(
    @InjectConnection() private readonly connection: Connection,
    private coreService: CoreService
  ) {}

  async getList(@Req() req) : Promise<any> {
    const { roleId, workspace } = req.user
    const filter_workspace = { isActive: true, deleted: false }
    if (workspace) {
      filter_workspace['_id'] = workspace
    } else {
      filter_workspace['name'] = process.env.WORKSPACE
    }
    const workspaceObj = await this.connection.collection('workspaces').findOne(filter_workspace)
    console.log('------workspaceObj------', workspaceObj)
    if (!workspaceObj) {
      throw new ForbiddenException('WorkspaceId is missing')
    }
    const search = []
    if (workspaceObj.isBaseUser) {
      const role = await this.coreService.getList({
        name: 'baseUserRole', single: true,
        search: [ { name: '_id', value: roleId }, { name: 'workspace', value: workspace } ]
      })
      if (!role) {
        throw new ForbiddenException('Хандах эрх тохируулаагүй байна.')
      }
			console.log('------role------', role)
      const rolemenuactions = await this.coreService.getList({
        name: 'baseUserRoleMenuActions',
        search: [ { name: 'roleId', value: role._id }, { name: 'workspace', value: workspace } ]
      })
      if (!rolemenuactions && (rolemenuactions && !rolemenuactions.rows)) {
        throw new ForbiddenException('Хандах эрх тохируулаагүй байна.')
      }
      for (const r of rolemenuactions.rows) {
        search.push({ name: '_id', value: r.workspaceMenu })
      }
    } else {
      if (workspaceObj.name === 'uberp_service') {
        const ub_menus = await this.getOnlineServiceMenus(req)
        for (const r of ub_menus) {
          search.push({ name: '_id', value: r })
        }
      }
    }
    console.log('------search------', search)
    const menus = await this.coreService.getList({
      name: "workspaceMenus",
      modelName: "Page",
      workspace: workspace,
      search: JSON.stringify(search),
      skipTotal: true,
      sorts: [{ value: 1, name: 'order', sortOrder: 0 }]
    })
    return menus
  }
  async getOnlineServiceMenus(@Req() req) : Promise<any> {
    const { _id } = req.user
    const serrole = await this.coreService.getList({
      name: 'serRole', single: true,
      search: [ { name: 'serUserId', value: _id } ]
    })
    if (!serrole || (serrole && !serrole.refRoleId)) {
      throw new ForbiddenException('Хандах эрх тохируулаагүй байна.')
    }
    const rolemenuactions = await this.coreService.getList({
      name: 'refSerRoleMenuAction',
      search: [ { name: 'refSerRoleId', value: serrole.refRoleId._id } ]
    })
    if (!rolemenuactions && (rolemenuactions && !rolemenuactions.rows)) {
      throw new ForbiddenException('Хандах эрх тохируулаагүй байна.')
    }
    const menus = []
    for (const r of rolemenuactions.rows) {
      menus.push(r.workspaceMenuIdId)
    }
    return menus
  }
}
