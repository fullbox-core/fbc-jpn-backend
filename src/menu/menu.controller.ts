import { Controller, Get, UseGuards, Req, HttpStatus, HttpException} from '@nestjs/common';
import { MenuService } from './menu.service'
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('menu')
export class MenuController {
    constructor(private menuService: MenuService) {}

    @UseGuards(JwtAuthGuard)
    @Get()
    getList(@Req() req: any){
        return this.menuService.getList(req)
    }
}
