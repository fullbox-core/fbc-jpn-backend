import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel, InjectConnection } from '@nestjs/mongoose';

import { Workspace, WorkspaceDocument } from '../schemas/workspace/workspace.schema';
import { Model, Connection, Types } from 'mongoose';
import { CoreService } from 'src/core/core.service';
import { HttpService } from '@nestjs/axios';
import { Observable } from 'rxjs';
import { Axios } from 'axios';
import * as _ from 'lodash'
@Injectable()
export class MainService {
    constructor(
        @InjectConnection() private readonly connection: Connection,
        @InjectModel(Workspace.name) private workspaceModel: Model<WorkspaceDocument>,
        private coreService: CoreService,
        private httpService: HttpService
    ) {}
    async findAll() {
        return await this.workspaceModel.find()
    }

    /**
     * @function 
     * Системийн нэвтрэх хуудасны тохиргоог core системээс татна.
     * 
     * @constant {string} workspaceName environment file дээр тохируулагдсан воркспэйсийн нэр 
     * 
     * @constant {string} requestUrl environment file дээр тохируулагдсан core системийн хүсэлт илгээх зам
     * 
     * @returns Хуудасны тохиргоог буцаана
     */
    async getLoginConfig() {
        try {
            const workspaceName = process.env.WORKSPACE
            // const requestUrl = process.env.LOGIN_CONFIG_URL
            const workspace = await this.connection.collection('workspaces').findOne({ name: workspaceName, isActive: true, deleted: false})
            const result = await this.coreService.getList({
                name: 'corePages',
                search: [
                    { name: 'workspace', value: workspace._id },
                    { name: 'type', value:  '619da1ad1fa41a65d8adbf55'}
                ],
                single: true
            })
            if(!result._id) {
                throw new HttpException('Page not found.', HttpStatus.FORBIDDEN)
            }
            const data = { workspaceId:workspace._id, config: null }
            const config = await this.coreService.getList({
                name: 'corePageConfig',
                search: [
                    { name: 'page', value: result._id }
                ],
                single: true
            })
            data.config = config
            return data
            // return (await axios.get( requestUrl + '/' + workspace._id)).data
        } catch (error) {
            throw new HttpException('Server error', HttpStatus.FORBIDDEN)
        }
       
    }
    /**
     * FSI lavlagaa
     * inumber болон password-оор токен авна
     * 
    */
    async getFsi(regNumber: string) {
        this.httpService.post(process.env.FSI_TOKEN_URL, null, {params: { inumber: process.env.FSI_INUMBER, password: process.env.FSI_PASSWORD}})
        //token авах сервис
        const response = await this.httpService.post(process.env.FSI_TOKEN_URL, null, {params: { inumber: process.env.FSI_INUMBER, password: process.env.FSI_PASSWORD}}).toPromise()
        const token = response.data.access_token
        //Регистреер лавлагаа авах сервис
        /**
         * @todo 
         * type=company || citizen
         * ямар үед компани төрлөөр ямар үед иргэн төрлөөр сервис дуудхыг шийдэх
        */
        const infoObjects = await this.httpService.get(encodeURI(process.env.FSI_URL+'?type=company&regnumber='+regNumber), { headers: {"Authorization" : `Bearer ${token}`} }).toPromise()
        const info = _.get(infoObjects, 'data.info')
        const objects = _.get(infoObjects, 'data.objects')
        const result = _.get(infoObjects, 'data.result')
        const data : any = {
            info,
            objects,
            result
        }
        // function getNewKey(key) {
        //     const index = key.indexOf('_')
        //     if (index !== -1) {
        //         const a = key.substr(index, 2)
        //         return key.replace(a, a.replace('_', '').toUpperCase())
        //     } else {
        //         return key
        //     }
        // }
        // for (const key in info) {
        //     let newKey = getNewKey(key)
        //     data.info[newKey] = info[key]
        // }
        // for (const object of objects) {
        //     const newObject = {}
        //     for (const key in object) {
        //         let newKey = getNewKey(key)
        //         newObject[newKey] = object[key]
        //     }
        //     data.objects.push(newObject)
        // }
        return data
    }

}
