import { Controller, Get, HttpStatus, Param, Req, Res } from '@nestjs/common';
import { MainService } from './main.service';
import { Response } from 'express';

@Controller('main')
export class MainController {
  constructor(private readonly mainService: MainService) {}
  @Get()
  async findAll() {
    return await this.mainService.findAll()
  }

  @Get('loginConfig')
  async getLoginConfig() {
    return await this.mainService.getLoginConfig()
  }

  @Get('fsi/:regNumber')
  async getFsi(@Param('regNumber') regNumber: string) {
    return await this.mainService.getFsi(regNumber)
  }
}
