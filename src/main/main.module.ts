import { Module } from '@nestjs/common';
import { MainController } from './main.controller';
import { MainService } from './main.service';
import { Workspace, WorkspaceSchema } from '../schemas/workspace/workspace.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { HttpModule } from '@nestjs/axios'
import { CoreModule } from 'src/core/core.module';

@Module({
  imports: [
    CoreModule,
    HttpModule,
    MongooseModule.forFeature([
      { name: Workspace.name, schema: WorkspaceSchema },
    ])
  ],
  controllers: [MainController],
  providers: [MainService]
})
export class MainModule {}
