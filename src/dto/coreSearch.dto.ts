import { Types } from "mongoose";

export class CoreSearchDto {
  name: string;
  value?: any;
  typeId?: Types.ObjectId;
}