export class aggregateDto {
  $match?: object;
  $limit?: number | Number;
  $skip?: number | Number;
  $sort?: object;
  $lookup?: object;
  $addFields?: object;
  $project?: object;
  $facet?: object;
  $count?: object | string;
  $group?: object;
  $unwind?: object | string;
  $unset? : object | string
}
