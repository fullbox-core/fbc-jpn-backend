import { Types } from "mongoose";

export class CoreGetOneDto {
  name: string;
  id: Types.ObjectId;
  schemaKey?: any;
  lookup?: boolean;
}
