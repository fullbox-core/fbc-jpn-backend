export class CoreError implements Error {
  name: string;
  message: string;
  stack?: string;
  caption: string;
}
