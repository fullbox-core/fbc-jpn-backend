import { Types } from "mongoose";

export class CoreSchemaDto {
  _id: Types.ObjectId;
  name: string;
  timestamps: boolean;
  caption: string;
  sortOrder: number;
  createdAt: Date;
  updatedAt: Date;
  isActive: boolean;
  deleted: boolean;
  deletedAt: Date
}
