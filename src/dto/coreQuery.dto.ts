import { Types } from "mongoose";
import { CoreSearchDto } from "./coreSearch.dto";
import { CoreSelectDto } from "./coreSelect.dto";
import { CoreSortDto } from "src/dto/coreSort.dto";

export class CoreQueryDto {
  name?: string;
  id?: Types.ObjectId;
  modelName?: string;
  workspace?: string;
  offset?: number;
  page?: number;
  limit?: number;
  search?: CoreSearchDto[] | string;
  select?: CoreSelectDto[];
  skipTotal?: boolean;
  single?: boolean;
  lookup?: boolean;
  user?: any;
  sorts?: CoreSortDto[];
  onlyTotal?: boolean;
  subCounts?: string[] | string;
}