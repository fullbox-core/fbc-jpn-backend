export class CoreSortDto {
  name: string;
  value?: any;
  sortOrder: number;
}