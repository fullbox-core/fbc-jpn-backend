import { DATETYPE, SEARCHTYPE } from "src/utils/core/core.enums";

export class ConvertDto {
  value: any;
  dataTypeName: string;
  dateType?: DATETYPE;
  searchTypeCode? : SEARCHTYPE
}