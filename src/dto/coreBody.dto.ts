export class CoreBodyDto {
  workspaceId?: string;
  id?: string;
  name?: string;
  user?: any;
  data: any;
}