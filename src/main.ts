import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
const fs = require('fs')
const join = require('path').join
const { createProxyMiddleware } = require('http-proxy-middleware')
let httpsOptions = null
if (process.env.SSL_PATH) {
  httpsOptions = {
    key: fs.readFileSync(join(process.env.SSL_PATH, process.env.SSL_KEY)),
    cert: fs.readFileSync(join(process.env.SSL_PATH, process.env.SSL_CRT)),
    ca: fs.readFileSync(join(process.env.SSL_PATH, process.env.SSL_CA)),
  };
}
async function bootstrap() {
  const app = await NestFactory.create(AppModule, { httpsOptions });
  app.enableCors();
  app.use('/core/upload', createProxyMiddleware({
    target: process.env.CORE_URL,
    changeOrigin: true,
    pathRewrite: {'^/core/upload' : '/coreapi/upload'},
    onProxyReq: function onProxyReq(proxyReq, req, res) {
      proxyReq.setHeader('workspace', process.env.WORKSPACE);
    }
  }))
  await app.listen(process.env.PORT || process.env.BASE_PORT);
}
bootstrap();
