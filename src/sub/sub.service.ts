import { HttpException, HttpStatus, Injectable, Req, Res } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { Types, Connection } from 'mongoose';
import { HttpService } from '@nestjs/axios';
import { CoreService } from 'src/core/core.service';
import _ = require("lodash")
import { deflateSync } from 'zlib';

@Injectable()
export class SubService {
	constructor(
    private coreService: CoreService,
    @InjectConnection() private readonly connection: Connection,
    private httpService: HttpService
  ) {}
  async getCitizenList(@Req() req) {
    const { offset, limit } = await this.paging(req)
    let filter : any = null
    filter = {
      deleted: false
    }
    const listQuery = JSON.parse(req.query.listQuery)
    if (listQuery.createdNumber) {
      filter.createdNumber = ''
      filter.createdNumber = new RegExp(listQuery.createdNumber, 'i')
    }
    if (listQuery.lastName) {
      filter.lastName = ''
      filter.lastName = new RegExp(listQuery.lastName, 'i')
    }
    if (listQuery.firstName) {
      filter.firstName = ''
      filter.firstName = new RegExp(listQuery.firstName, 'i')
    }
    if (listQuery.address) {
      filter.address = ''
      filter.address = new RegExp(listQuery.address, 'i')
    }
    if (listQuery.phoneNumber) {
      filter.phoneNumber = ''
      filter.phoneNumber = new RegExp(listQuery.phoneNumber, 'i')
    }
    if (listQuery.workType) {
      filter.workType = ''
      filter.workType = new Types.ObjectId(listQuery.workType)
    }
    if (listQuery.email) {
      filter.email = ''
      filter.email = new RegExp(listQuery.email, 'i')
    }
    if (listQuery.maxSalary) {
      filter.maxSalary = ''
      filter.maxSalary = parseInt(listQuery.maxSalary)
    }
    if (req.query.type) {
      filter.citizenType = new Types.ObjectId(req.query.type)
    }
    if (req.query.userId) {
      filter.baseUserId = new Types.ObjectId(req.query.userId)
    }
    const result = await this.connection.collection('hhCitizen').aggregate([
      { $match: filter },
      { $lookup: { from: 'hhEducation', localField: '_id', foreignField: 'citizenId', as: 'hhEducation' } },
      { $lookup: { from: 'hhEmergencyContact', localField: '_id', foreignField: 'citizenId', as: 'hhEmergencyContact' } },
			{ $lookup: { from: 'hhBeforeGoing', localField: '_id', foreignField: 'citizenId', as: 'hhBeforeGoing' } },
      { $lookup: { from: 'hhRefWorkforceType', localField: 'workType', foreignField: '_id', as: 'workType' } },
      { $unwind: { path: '$workType', preserveNullAndEmptyArrays: true } },
			{ $lookup: { from: 'hhHistoryWork', localField: '_id', foreignField: 'citizenId', as: 'hhHistoryWork' } },
			{ $lookup: { from: 'hhAnotherCertificates', localField: '_id', foreignField: 'citizenId', as: 'hhAnotherCertificates' } },
			{ $lookup: { from: 'hhSkillCheck', localField: '_id', foreignField: 'citizenId', as: 'hhSkillCheck' } },
			{ $lookup: { from: 'hhWantedPlace', localField: '_id', foreignField: 'citizenId', as: 'hhWantedPlace' } },
      { $group: {
        _id: '$_id',
        updatedAt: {$first: '$updatedAt'},
        deleted: {$first: '$deleted'},
        createdAt: {$first: '$createdAt'},
        anotherCertificate: {$first: '$anotherCertificate'},
        intrestedUser: {$first: '$intrestedUser'},
        workPlace: {$first: '$workPlace'},
        orgShow: {$first: '$orgShow'},
        createdNumber: {$first: '$createdNumber'},
        firstName: {$first: '$firstName'},
        lastName: {$first: '$lastName'},
        age: {$first: '$age'},
        phoneNumber: {$first: '$phoneNumber'},
        email: {$first: '$email'},
        baseUserId: {$first: '$baseUserId'},
        gender: {$first: '$gender'},
        attachmentPassword: {$first: '$attachmentPassword'},
        attachmentPhoto: {$first: '$attachmentPhoto'},
        aimagCity: {$first: '$aimagCity'},
        attachmentAnotherCetrificate: {$first: '$attachmentAnotherCetrificate'},
        isActive: {$first: '$isActive'},
        attachmentHealt: {$first: '$attachmentHealt'},
        attachmentEducation: {$first: '$attachmentEducation'},
        attachmentBeforeGoing: {$first: '$attachmentBeforeGoing'},
        attachmentJFT: {$first: '$attachmentJFT'},
        attachmentJLPT: {$first: '$attachmentJLPT'},
        address: {$first: '$address'},
        telephone: {$first: '$telephone'},
        marred: {$first: '$marred'},
        marredShowOrg: {$first: '$marredShowOrg'},
        childCount: {$first: '$childCount'},
        childCountShowOrg: {$first: '$childCountShowOrg'},
        beforeGoing: {$first: '$beforeGoing'},
        passedExam: {$first: '$passedExam'},
        skillTrainee: {$first: '$skillTrainee'},
        historyWork: {$first: '$historyWork'},
        langCheckJLPTNumber: {$first: '$langCheckJLPTNumber'},
        langCheckJLPTDate: {$first: '$langCheckJLPTDate'},
        langCheckJLPTPoint: {$first: '$langCheckJLPTPoint'},
        langCheckJFTNumber: {$first: '$langCheckJFTNumber'},
        langCheckJFTPoint: {$first: '$langCheckJFTPoint'},
        langCheckJFTDate: {$first: '$langCheckJFTDate'},
        healthOrg: {$first: '$healthOrg'},
        healthCond: {$first: '$healthCond'},
        tall: {$first: '$tall'},
        healthDate: {$first: '$healthDate'},
        wantPlace: {$first: '$wantPlace'},
        basicSalReq: {$first: '$basicSalReq'},
        maxSalary: {$first: '$maxSalary'},
        minSalary: {$first: '$minSalary'},
        viewCount: {$first: '$viewCount'},
        citizenType: {$first: '$citizenType'},
        register: {$first: '$register'},
        weigth: {$first: '$weigth'},
        attachmentSkill: {$first: '$attachmentSkill'},
        createdBy: {$first: '$createdBy'},
        updatedBy: {$first: '$updatedBy'},
        hhEducation: {$first: '$hhEducation'},
        hhEmergencyContact: {$first: '$hhEmergencyContact'},
        hhBeforeGoing: {$first: '$hhBeforeGoing'},
        workType: {$first: '$workType'},
        hhAnotherCertificates: {$first: '$hhAnotherCertificates'},
        hhHistoryWork: {$first: '$hhHistoryWork'},
        hhSkillCheck: {$first: '$hhSkillCheck'},
        hhWantedPlace: {$first: '$hhWantedPlace'}
      } },
			{ $lookup: { from: 'hhinterview', localField: '_id', foreignField: 'citizenId', as: 'hhinterview' } },
			{ $lookup: { from: 'baseUser', localField: 'createdBy', foreignField: '_id', as: 'baseUser' } },
      { $unwind: { path: '$baseUser', preserveNullAndEmptyArrays: true } },
      { $lookup: { from: 'hhRefType', localField: 'citizenType', foreignField: '_id', as: 'citizenType' } },
      { $unwind: { path: '$citizenType', preserveNullAndEmptyArrays: true } },
      { $lookup: { from: 'hhGender', localField: 'gender', foreignField: '_id', as: 'gender' } },
      { $unwind: { path: '$gender', preserveNullAndEmptyArrays: true } },
      { $lookup: { from: 'refAimagCity', localField: 'aimagCity', foreignField: '_id', as: 'aimagCity' } },
      { $unwind: { path: '$aimagCity', preserveNullAndEmptyArrays: true } },
      { $sort: { createdAt: -1 } },
      { $facet: {
        rows: [
          { $skip: offset },
          { $limit: limit }
        ],
        totalCount: [
          { $count: 'count' }
        ]
      } }
    ]).toArray()
    for(const item of result[0].rows) {
      item.hhEducation = item.hhEducation.filter(c => c.deleted === false)
      item.hhEmergencyContact = item.hhEmergencyContact.filter(c => c.deleted === false)
      item.hhBeforeGoing = item.hhBeforeGoing.filter(c => c.deleted === false)
      item.hhAnotherCertificates = item.hhAnotherCertificates.filter(c => c.deleted === false)
      item.hhWantedPlace = item.hhWantedPlace.filter(c => c.deleted === false)
      item.hhSkillCheck = item.hhSkillCheck.filter(c => c.deleted === false)
      item.hhHistoryWork = item.hhHistoryWork.filter(c => c.deleted === false)
    } 
    let data = this.dataHelper(result)
    return data
  }
	async postCitizenList(@Req() req) {
		const body : any = req.body
    let user : any = null
		user = {
      name: body.createdNumber,
      firstName: body.firstName,
      lastName: body.lastName,
      passwordHash: 'Hh' + body.phoneNumber,
      roleId: '62134e53f5b29f312ccf2ff4',
      workspace: '6204e757c2b419541ccab11c',
      email: body.email,
    }
    const baseUser = await this.coreService.save({ name: 'baseUser', data: user })
    let receive : any = null
		receive = {
      createdNumber: body.createdNumber,
      register: body.register,
      firstName: body.firstName,
      lastName: body.lastName,
      age: body.age,
      phoneNumber: body.phoneNumber,
      email: body.email,
      baseUserId: baseUser._id,
      gender: body.gender,
      aimagCity: body.aimagCity,
      attachmentPhoto: body.attachmentPhoto,
      attachmentPassword: body.attachmentPassword,
      attachmentHealt: body.attachmentHealt,
      attachmentEducation: body.attachmentEducation,
      attachmentBeforeGoing: body.attachmentBeforeGoing,
      attachmentAnotherCetrificate: body.attachmentAnotherCetrificate,
      attachmentSkill: body.attachmentSkill,
      attachmentJFT: body.attachmentJFT,
      attachmentJLPT: body.attachmentJLPT,
      address: body.address,
      telephone: body.telephone,
      marred: body.marred,
      workType: body.workType._id,
      marredShowOrg: body.marredShowOrg,
      childCount: body.childCount,
      childCountShowOrg: body.childCountShowOrg,
      beforeGoing: body.beforeGoing,
      passedExam: body.passedExam,
      skillTrainee: body.skillTrainee,
      historyWork: body.historyWork,
      langCheckJLPTNumber: body.langCheckJLPTNumber,
      langCheckJLPTDate: body.langCheckJLPTDate,
      langCheckJLPTPoint: body.langCheckJLPTPoint,
      langCheckJFTNumber: body.langCheckJFTNumber,
      langCheckJFTPoint: body.langCheckJFTPoint,
      langCheckJFTDate: body.langCheckJFTDate,
      healthOrg: body.healthOrg,
      healthCond: body.healthCond,
      tall: body.tall,
      weigth: body.weigth,
      healthDate: body.healthDate,
      wantPlace: body.wantPlace,
      basicSalReq: body.basicSalReq,
      maxSalary: body.maxSalary,
      minSalary: body.minSalary,
      citizenType: '6215afdb510d3e2d94e88c5b',
      createdAt: new Date(),
      createdBy: req.user._id,
    }
    const data = await this.coreService.save({ name: 'hhCitizen', data: receive })
    let citizenType : any = null
		citizenType = {
      citizenId: data._id,
      statusId: new Types.ObjectId('6215afdb510d3e2d94e88c5b'),
      organizationId: null,
      createdBy: req.user._id,
      createdAt: new Date(),
    }
    await this.coreService.save({ name: 'hhCitizenType', data: citizenType })
    for (let item of body.hhEducation) {
      let edu = {
        startDate: item.startDate,
        endDate: item.endDate,
        address: item.address,
        degree: item.degree,
        citizenId: data._id
      }
      await this.coreService.save({ name: 'hhEducation', data: edu })
    }
    for (let value of body.hhEmergencyContact) {
      let contact = {
        who: value.who,
        lastName: value.lastName,
        firstName: value.firstName,
        phone: value.phone,
        citizenId: data._id
      }
      await this.coreService.save({ name: 'hhEmergencyContact', data: contact })
    }
    if (body.beforeGoing) {
      for (let value of body.hhBeforeGoing) {
        let going = {
          startDate: value.startDate,
          endDate: value.endDate,
          aimagCity: value.aimagCity,
          direction: value.direction,
          citizenId: data._id
        }
        await this.coreService.save({ name: 'hhBeforeGoing', data: going })
      }
    }
    if (body.historyWork) {
      for (let value of body.hhHistoryWork) {
        let work = {
          yearCount: value.yearCount,
          direction: value.direction,
          aimagCity: value.aimagCity,
          workName: value.workName,
          address: value.address,
          citizenId: data._id
        }
        await this.coreService.save({ name: 'hhHistoryWork', data: work })
      }
    }
    if (body.anotherCertificate) {
      for (let value of body.hhAnotherCertificates) {
        let cetrificate = {
          about: value.about,
          date: value.date,
          organization: value.organization,
          citizenId: data._id
        }
        await this.coreService.save({ name: 'hhAnotherCertificates', data: cetrificate })
      }
    }
    for (let value of body.hhSkillCheck) {
      let skill = {
        number: value.number,
        date: value.date,
        point: value.point,
        citizenId: data._id
      }
      await this.coreService.save({ name: 'hhSkillCheck', data: skill })
    }
    if (body.wantPlace) {
      for (let value of body.hhWantedPlace) {
        let place = {
          region: value.region,
          aimagCity: value.aimagCity,
          soumDiscrict: value.soumDiscrict,
          citizenId: data._id
        }
        await this.coreService.save({ name: 'hhWantedPlace', data: place })
      }
    }
  }
  async getRefTypeList(@Req() req) {
    const { offset, limit } = await this.paging(req)
    const result = await this.connection.collection('hhRefType').aggregate([
      { $match: { deleted: false } },
      { $sort: { createdAt: 1 } },
      { $facet: {
        rows: [
          { $skip: offset },
          { $limit: limit }
        ],
        totalCount: [
          { $count: 'count' }
        ] 
      } }
    ]).toArray()
    let data = this.dataHelper(result)
    return data
  }
  async getCitizenButtonCount(@Req() req) {
    const result = await this.connection.collection('hhCitizen').aggregate([
      { $match: { deleted: false } },
      { $lookup: { from: 'hhRefType', localField: 'citizenType', foreignField: '_id', as: 'citizenType' } },
      { $unwind: { path: '$citizenType', preserveNullAndEmptyArrays: true } },
      { $sort: { createdAt: 1 } },
      { $group: { '_id': '$citizenType._id', 'content': { '$push': '$citizenType' } }},
    ]).toArray()
    return result
  }
  async getCitizenType(@Req() req) {
    let filter : any = null
    filter = {
      deleted: false
    }
    if (req.query.citizenId) {
      filter.citizenId = new Types.ObjectId(req.query.citizenId)
    }
    const { offset, limit } = await this.paging(req)
    const result = await this.connection.collection('hhCitizenType').aggregate([
      { $match: filter },
      { $lookup: { from: 'hhOrganization', localField: 'organizationId', foreignField: '_id', as: 'hhOrganization' } },
      { $unwind: { path: '$hhOrganization', preserveNullAndEmptyArrays: true } },
      { $sort: { createdAt: -1 } },
      { $facet: {
        rows: [
          { $skip: offset },
          { $limit: limit }
        ],
        totalCount: [
          { $count: 'count' }
        ] 
      } }
    ]).toArray()
    let data = this.dataHelper(result)
    return data
  }
  async getWorkPlace(@Req() req) {
    const body : any = req.query
    let filter : any = null
    filter = {
      deleted: false
    }
    if (req.user._id) {
      filter.createdUser = new Types.ObjectId(req.user._id)
    }
    const result = await this.connection.collection('hhWorkplace').aggregate([
      { $match: filter },
      { $sort: { createdAt: -1 } }
    ]).toArray()
    return result
  }
  async getWorkPlaceDialog(@Req() req) {
    const body : any = req.query
    console.log(body, '-body-')
    let filter : any = null
    filter = {
      deleted: false
    }
    if (body.workId) {
      filter._id = new Types.ObjectId(body.workId)
    }
    const result = await this.connection.collection('hhWorkplace').aggregate([
      { $match: filter },
      { $sort: { createdAt: -1 } }
    ]).toArray()
    return result
  }
  async getIntrestedOrg(@Req() req) {
    let filter : any = null
    filter = {
      deleted: false
    }
    if (req.query.userId) {
      filter.baseUserId = new Types.ObjectId(req.query.userId)
    }
    const { offset, limit } = await this.paging(req)
    const result = await this.connection.collection('hhOrganization').aggregate([
      { $match: filter },
      { $sort: { createdAt: -1 } },
      { $facet: {
        rows: [
          { $skip: offset },
          { $limit: limit }
        ]
      } }
    ]).toArray()
    let data = this.dataHelper(result)
    return data
  }
  async postCitizenType(@Req() req) {
    const body : any = req.body
    body.citizenType = '6215a374510d3e2d94e887f9'
    let receive : any = null
    const org = await this.connection.collection('hhOrganization').findOne({ baseUserId: new Types.ObjectId(req.user._id) })
		receive = {
      citizenId: new Types.ObjectId(body._id),
      statusId: new Types.ObjectId('6215a374510d3e2d94e887f9'),
      organizationId: org._id,
      createdBy: req.user._id,
      createdAt: new Date(),
    }
    body.intrestedUser = req.user._id
    await this.coreService.save({ name: 'hhCitizenType', data: receive })
    await this.coreService.save({ name: 'hhCitizen', data: body })
  }
  async postCitizenTypeCancel(@Req() req) {
    const body : any = req.body
    body.citizenType = '6215afdb510d3e2d94e88c5b'
    body.intrestedUser = ''
    body.workPlace = ''
    await this.coreService.save({ name: 'hhCitizen', data: body })
  }
  async postCitizenTypeConf(@Req() req) {
    const body : any = req.body
    body.citizenType = '6215a3a5510d3e2d94e88870'
    await this.coreService.save({ name: 'hhCitizen', data: body })
  }
  async postConcludeContract(@Req() req) {
    const body : any = req.body
    body.citizenType = '6215a3cf510d3e2d94e88969'
    await this.coreService.save({ name: 'hhCitizen', data: body })
  }
  async postContractCreated(@Req() req) {
    const body : any = req.body
    body.citizenType = '6215a3e2510d3e2d94e889bc'
    await this.coreService.save({ name: 'hhCitizen', data: body })
  }
  async postSaveTalkReq(@Req() req) {
    const body : any = req.body
    let talk : any = null
    talk = {
      time: body.interview.time,
      description: body.interview.description,
      citizenId: body._id,
      organizationUser: req.user._id,
      createdAt: new Date(),
      createdBy: req.user._id,
    }
    await this.coreService.save({ name: 'hhinterview', data: talk })
    body.citizenType = '624f9efa7b4dda50e4b8129c'
    await this.coreService.save({ name: 'hhCitizen', data: body })
  }
  async postPassed(@Req() req) {
    const body : any = req.body
    body.citizenType = '6215a3b1510d3e2d94e888c3'
    await this.coreService.save({ name: 'hhCitizen', data: body })
  }
  async postgotJob(@Req() req) {
    const body : any = req.body
    body.citizenType = '6260db996ca0c70878537b40'
    await this.coreService.save({ name: 'hhCitizen', data: body })
  }
  async postFailed(@Req() req) {
    const body : any = req.body
    body.citizenType = '6215afdb510d3e2d94e88c5b'
    await this.coreService.save({ name: 'hhCitizen', data: body })
  }
  dataHelper(result: any) {
    const data = { rows: [], count: 0 }
    if (!this.isNullOrEmpty(result)) {
      data.rows = this.isNullOrEmpty(result[0].rows) ? [] : result[0].rows
      data.count = this.isNullOrEmpty(result[0].totalCount) ? 0 : result[0].totalCount[0].count
    }
    return data
  }
  isNullOrEmpty(values) {
    return values === undefined || values === null || values.length === 0
  }
	async paging(@Req() req) {
    const limit = parseInt(req.query.limit)
    const offset = (parseInt(req.query.offset) - 1) * limit
    return { offset, limit }
  }
  async filter(@Req() req) {
    const defaultFilter = { deleted: false }
    let where: any = {
      $and: [{ deleted: false }]
    }
    const searchList = JSON.parse(req.query.searches)
    if (searchList && searchList.length > 0) {
      for (const search of searchList) {
        if (!search.val) continue
        if (search.field === 'checkTypeId') {
          const tmp = await this.connection.collection('suuRefCheckPerformance').find({ checkTypeId: new Types.ObjectId(search.val), deleted: false }).toArray()
          const reqIds = tmp.map(item => { return item._id })
          where.$and.push({ _id: { $in: reqIds } })
        }
        if (search.field === 'tankId') {
          const tmp = await this.connection.collection('suuReceiveTank').find({ tankId: new Types.ObjectId(search.val), deleted: false }).toArray()
          const reqIds = tmp.map(item => { return item._id })
          where.$and.push({ _id: { $in: reqIds } })
        }
        else if (search.field === 'deliveryNumber') {
          const tmp = await this.connection.collection('suuReceiveTank').find({ deliveryNumber: parseInt(search.val), deleted: false }).toArray()
          const reqIds = tmp.map(item => { return item._id })
          where.$and.push({ _id: { $in: reqIds } })
        } else if (search.field === 'size') {
          const tmp = await this.connection.collection('suuReceiveTank').find({ size: parseInt(search.val), deleted: false }).toArray()
          const reqIds = tmp.map(item => { return item._id })
          where.$and.push({ _id: { $in: reqIds } })
        }
      }
    }
    return { where }
  }
  async putCitizen(@Req() req) {
    const body : any = req.body
    let receive : any = null
		receive = {
      _id: body._id,
      createdNumber: body.createdNumber,
      register: body.register,
      workType: body.workType._id,
      firstName: body.firstName,
      lastName: body.lastName,
      attachmentHealt: body.attachmentHealt,
      attachmentEducation: body.attachmentEducation,
      attachmentBeforeGoing: body.attachmentBeforeGoing,
      attachmentPhoto: body.attachmentPhoto,
      attachmentAnotherCetrificate: body.attachmentAnotherCetrificate,
      attachmentSkill: body.attachmentSkill,
      attachmentJFT: body.attachmentJFT,
      attachmentJLPT: body.attachmentJLPT,
      age: body.age,
      aimagCity: body.aimagCity,
      anotherCertificate: body.anotherCertificate,
      phoneNumber: body.phoneNumber,
      email: body.email,
      baseUserId: body.baseUser._id,
      gender: body.gender,
      attachmentPassword: body.attachmentPassword,
      address: body.address,
      telephone: body.telephone,
      marred: body.marred,
      marredShowOrg: body.marredShowOrg,
      childCount: body.childCount,
      childCountShowOrg: body.childCountShowOrg,
      beforeGoing: body.beforeGoing,
      passedExam: body.passedExam,
      skillTrainee: body.skillTrainee,
      historyWork: body.historyWork,
      langCheckJLPTNumber: body.langCheckJLPTNumber,
      langCheckJLPTDate: body.langCheckJLPTDate,
      langCheckJLPTPoint: body.langCheckJLPTPoint,
      langCheckJFTNumber: body.langCheckJFTNumber,
      langCheckJFTPoint: body.langCheckJFTPoint,
      langCheckJFTDate: body.langCheckJFTDate,
      healthOrg: body.healthOrg,
      healthCond: body.healthCond,
      tall: body.tall,
      weigth: body.weigth,
      healthDate: body.healthDate,
      wantPlace: body.wantPlace,
      basicSalReq: body.basicSalReq,
      maxSalary: body.maxSalary,
      minSalary: body.minSalary,
      citizenType: body.citizenType._id,
      updatedAt: new Date(),
      updatedBy: req.user._id,
    }
    const data = await this.coreService.save({ name: 'hhCitizen', data: receive })
    for (let item of body.hhEducation) {
      if (item && item._id) {
        let edu = {
          _id: item._id,
          startDate: item.startDate,
          endDate: item.endDate,
          address: item.address,
          degree: item.degree,
          citizenId: data._id
        }
        await this.coreService.save({ name: 'hhEducation', data: edu })
      } else {
        let edu = {
          startDate: item.startDate,
          endDate: item.endDate,
          address: item.address,
          degree: item.degree,
          citizenId: data._id
        }
        await this.coreService.save({ name: 'hhEducation', data: edu })
      }
    }
    for (let value of body.hhEmergencyContact) {
      if (value && value._id) {
        let contact = {
          _id: value._id,
          who: value.who,
          lastName: value.lastName,
          firstName: value.firstName,
          phone: value.phone,
          citizenId: data._id
        }
        await this.coreService.save({ name: 'hhEmergencyContact', data: contact })
      } else {
        let contact = {
          who: value.who,
          lastName: value.lastName,
          firstName: value.firstName,
          phone: value.phone,
          citizenId: data._id
        }
        await this.coreService.save({ name: 'hhEmergencyContact', data: contact })
      }
    }
    if (body.beforeGoing) {
      for (let value of body.hhBeforeGoing) {
        if (value && value._id) {
          let going = {
            _id: value._id,
            startDate: value.startDate,
            endDate: value.endDate,
            aimagCity: value.aimagCity,
            direction: value.direction,
            citizenId: data._id
          }
          await this.coreService.save({ name: 'hhBeforeGoing', data: going })
        } else {
          let going = {
            startDate: value.startDate,
            endDate: value.endDate,
            aimagCity: value.aimagCity,
            direction: value.direction,
            citizenId: data._id
          }
          await this.coreService.save({ name: 'hhBeforeGoing', data: going })
        }
      }
    }
    if (body.historyWork) {
      for (let value of body.hhHistoryWork) {
        if (value && value._id) {
          let work = {
            _id: value._id,
            yearCount: value.yearCount,
            direction: value.direction,
            aimagCity: value.aimagCity,
            workName: value.workName,
            address: value.address,
            citizenId: data._id
          }
          await this.coreService.save({ name: 'hhHistoryWork', data: work })
        } else {
          let work = {
            yearCount: value.yearCount,
            direction: value.direction,
            aimagCity: value.aimagCity,
            workName: value.workName,
            address: value.address,
            citizenId: data._id
          }
          await this.coreService.save({ name: 'hhHistoryWork', data: work })
        }
      }
    }
    if (body.anotherCertificate) {
      for (let value of body.hhAnotherCertificates) {
        if (value && value._id) {
          let cetrificate = {
            _id: value._id,
            about: value.about,
            date: value.date,
            organization: value.organization,
            citizenId: data._id
          }
          await this.coreService.save({ name: 'hhAnotherCertificates', data: cetrificate })
        } else {
          let cetrificate = {
            about: value.about,
            date: value.date,
            organization: value.organization,
            citizenId: data._id
          }
          await this.coreService.save({ name: 'hhAnotherCertificates', data: cetrificate })
        }
      }
    }
    for (let value of body.hhSkillCheck) {
      if (value && value._id) {
        let skill = {
          _id: value._id,
          number: value.number,
          point: value.point,
          date: value.date,
          citizenId: data._id
        }
        await this.coreService.save({ name: 'hhSkillCheck', data: skill })
      } else {
        let skill = {
          number: value.number,
          date: value.date,
          point: value.point,
          citizenId: data._id
        }
        await this.coreService.save({ name: 'hhSkillCheck', data: skill })
      }
    }
    if (body.wantPlace) {
      for (let value of body.hhWantedPlace) {
        if (value && value._id) {
          let place = {
            _id: value._id,
            region: value.region,
            aimagCity: value.aimagCity,
            soumDiscrict: value.soumDiscrict,
            citizenId: data._id
          }
          await this.coreService.save({ name: 'hhWantedPlace', data: place })
        } else {
          let place = {
            region: value.region,
            aimagCity: value.aimagCity,
            soumDiscrict: value.soumDiscrict,
            citizenId: data._id
          }
          await this.coreService.save({ name: 'hhWantedPlace', data: place })
        }
      }
    }
  }
  async removeEducation(@Req() req) {
    const body : any = req.body
    await this.coreService.delete({ name: 'hhEducation', data: body._id })
  }
  async removeContact(@Req() req) {
    const body : any = req.body
    await this.coreService.delete({ name: 'hhEmergencyContact', data: body._id })
  }
  async removeBeforeGoing(@Req() req) {
    const body : any = req.body
    await this.coreService.delete({ name: 'hhBeforeGoing', data: body._id })
  }
  async removeWork(@Req() req) {
    const body : any = req.body
    await this.coreService.delete({ name: 'hhHistoryWork', data: body._id })
  }
  async removeCetrificate(@Req() req) {
    const body : any = req.body
    await this.coreService.delete({ name: 'hhAnotherCertificates', data: body._id })
  }
  async removeSkillCheck(@Req() req) {
    const body : any = req.body
    await this.coreService.delete({ name: 'hhSkillCheck', data: body._id })
  }
  async removeWantPlace(@Req() req) {
    const body : any = req.body
    await this.coreService.delete({ name: 'hhWantedPlace', data: body._id })
  }
  //Байгууллага
  async getOrganizationList(@Req() req) {
    const { offset, limit } = await this.paging(req)
    let filter : any = null
    filter = {
      deleted: false
    }
    if (req.query.userId) {
      filter.baseUserId = new Types.ObjectId(req.query.userId)
    }
    const listQuery = JSON.parse(req.query.listQuery)
    if (listQuery.name) {
      filter.name = ''
      filter.name = new RegExp(listQuery.name, 'i')
    }
    if (listQuery.aimagCity) {
      filter.aimagCity = ''
      filter.aimagCity = new RegExp(listQuery.aimagCity, 'i')
    }
    if (listQuery.address) {
      filter.address = ''
      filter.address = new RegExp(listQuery.address, 'i')
    }
    if (listQuery.email) {
      filter.email = ''
      filter.email = new RegExp(listQuery.email, 'i')
    }
    if (listQuery.phoneNumber) {
      filter.phoneNumber = ''
      filter.phoneNumber = parseInt(listQuery.phoneNumber)
    }
    if (listQuery.startDate) {
      filter.contractStartDate = ''
      filter.contractStartDate = new Date(listQuery.startDate)
    }
    if (listQuery.endDate) {
      filter.contractEndDate = ''
      filter.contractEndDate = new Date(listQuery.endDate)
    }
    if (listQuery.refCategory) {
      filter.category = ''
      filter.category = new Types.ObjectId(listQuery.refCategory)
    }
    if (listQuery.workforceType) {
      filter.workforceType = ''
      filter.workforceType = new Types.ObjectId(listQuery.workforceType)
    }
    if (listQuery.contractType) {
      filter.contractType = ''
      filter.contractType = new Types.ObjectId(listQuery.contractType)
    }
    const result = await this.connection.collection('hhOrganization').aggregate([
      { $match: filter },
      { $lookup: { from: 'hhRefOrgCategory', localField: 'category', foreignField: '_id', as: 'category' } },
      { $unwind: { path: '$category', preserveNullAndEmptyArrays: true } },
      { $lookup: { from: 'hhRefWorkforceType', localField: 'workforceType', foreignField: '_id', as: 'workforceType' } },
      { $unwind: { path: '$workforceType', preserveNullAndEmptyArrays: true } },
      { $lookup: { from: 'hhRefContractType', localField: 'contractType', foreignField: '_id', as: 'contractType' } },
      { $unwind: { path: '$contractType', preserveNullAndEmptyArrays: true } },
      { $lookup: { from: 'baseUser', localField: 'baseUserId', foreignField: '_id', as: 'baseUser' } },
      { $unwind: { path: '$baseUser', preserveNullAndEmptyArrays: true } },
      { $sort: { createdAt: -1 } },
      { $facet: {
        rows: [
          { $skip: offset },
          { $limit: limit }
        ],
        totalCount: [
          { $count: 'count' }
        ] 
      } }
    ]).toArray()
    let data = this.dataHelper(result)
    return data
  }
  async getRefCategory() {
    let filter : any = null
    filter = {
      deleted: false
    }
    const result = await this.connection.collection('hhRefOrgCategory').aggregate([
      { $match: filter },
    ]).toArray()
    return result
  }
  async gethhWorkforceTypes () {
    let filter : any = null
    filter = {
      deleted: false
    }
    const result = await this.connection.collection('hhRefWorkforceType').aggregate([
      { $match: filter },
    ]).toArray()
    return result
  }
  async getcontractTypes () {
    let filter : any = null
    filter = {
      deleted: false
    }
    const result = await this.connection.collection('hhRefContractType').aggregate([
      { $match: filter },
    ]).toArray()
    return result
  }
  async postOrganization(@Req() req) {
    const body : any = req.body
    let user : any = null
		user = {
      name: body.userName,
      firstName: body.name,
      lastName: body.category.name,
      passwordHash: body.password,
      roleId: '62134e68f5b29f312ccf3019',
      workspace: '6204e757c2b419541ccab11c',
      email: body.email,
      createdAt: new Date(),
      createdBy: req.user._id,
    }
    const baseUser = await this.coreService.save({ name: 'baseUser', data: user })
    let receive : any = null
		receive = {
      name: body.name,
      category: body.category._id,
      workforceType: body.workforceType._id,
      aimagCity: body.aimagCity,
      address: body.address,
      email: body.email,
      baseUserId: baseUser._id,
      phoneNumber: body.phoneNumber,
      contractStartDate: body.contractStartDate,
      contractEndDate: body.contractEndDate,
      contractType: body.contractType._id,
      createdAt: new Date(),
      createdBy: req.user._id,
    }
    const data = await this.coreService.save({ name: 'hhOrganization', data: receive })
    return data
  }
  async putOrganization(@Req() req) {
    const body : any = req.body
    let receive : any = null
		receive = {
      _id: body._id,
      name: body.name,
      category: body.category._id,
      workforceType: body.workforceType._id,
      aimagCity: body.aimagCity,
      address: body.address,
      email: body.email,
      baseUserId: body.baseUserId,
      phoneNumber: body.phoneNumber,
      contractStartDate: body.contractStartDate,
      contractEndDate: body.contractEndDate,
      contractType: body.contractType._id,
      updatedAt: new Date(),
      updatedBy: req.user._id,
    }
    await this.coreService.save({ name: 'hhOrganization', data: receive })
    // return data
  }
}
