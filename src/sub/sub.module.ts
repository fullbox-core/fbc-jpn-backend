import { Module } from '@nestjs/common';
import { SubService } from './sub.service';
import { SubController } from './sub.controller';
import { CoreModule } from 'src/core/core.module';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [CoreModule, HttpModule],
  controllers: [SubController],
  providers: [SubService],
  exports: [SubService]
})
export class SubModule {}
