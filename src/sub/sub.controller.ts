import { Controller, UseGuards, Get, Post, Delete, Body, Param, Req, Res, HttpException, HttpStatus } from '@nestjs/common'
import { CoreService } from 'src/core/core.service'
import { SubService } from './sub.service'
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard'

@Controller('sub')
export class SubController {
  constructor(private coreService: CoreService, private subService: SubService ) { }

  @UseGuards(JwtAuthGuard)
  @Get('hhCitizenGet')
  async getCitizen(@Req() req): Promise<any> {
    return await this.subService.getCitizenList(req)
  }

	@UseGuards(JwtAuthGuard)
  @Post('hhCitizenPost')
  async postCitizen(@Req() req): Promise<any> {
    return await this.subService.postCitizenList(req)
  }

  @UseGuards(JwtAuthGuard)
  @Get('hhRefTypeGet')
  async getRefType(@Req() req): Promise<any> {
    return await this.subService.getRefTypeList(req)
  }

  @UseGuards(JwtAuthGuard)
  @Get('hhCitizenTypeGet')
  async getCitizenType(@Req() req): Promise<any> {
    return await this.subService.getCitizenType(req)
  }
  
  @UseGuards(JwtAuthGuard)
  @Get('hhintrestedOrg')
  async getIntrestedOrg(@Req() req): Promise<any> {
    return await this.subService.getIntrestedOrg(req)
  }
  @UseGuards(JwtAuthGuard)
  @Get('hhCitizenButtonCount')
  async getCitizenButtonCount(@Req() req): Promise<any> {
    return await this.subService.getCitizenButtonCount(req)
  }
  @UseGuards(JwtAuthGuard)
  @Get('hhWorkPlace')
  async getWorkPlace(@Req() req): Promise<any> {
    return await this.subService.getWorkPlace(req)
  }

  @UseGuards(JwtAuthGuard)
  @Get('workPlaceDialog')
  async getWorkPlaceDialog(@Req() req): Promise<any> {
    return await this.subService.getWorkPlaceDialog(req)
  }

  @UseGuards(JwtAuthGuard)
  @Post('hhCitizenType')
  async postCitizenType(@Req() req): Promise<any> {
    return await this.subService.postCitizenType(req)
  }

  @UseGuards(JwtAuthGuard)
  @Post('hhCitizenTypeCancel')
  async postCitizenTypeCancel(@Req() req): Promise<any> {
    return await this.subService.postCitizenTypeCancel(req)
  }
  
  @UseGuards(JwtAuthGuard)
  @Post('hhCitizenTypeConf')
  async postCitizenTypeConf(@Req() req): Promise<any> {
    return await this.subService.postCitizenTypeConf(req)
  }
  
  @UseGuards(JwtAuthGuard)
  @Post('hhConcludeContract')
  async postConcludeContract(@Req() req): Promise<any> {
    return await this.subService.postConcludeContract(req)
  }
  
  @UseGuards(JwtAuthGuard)
  @Post('hhContractCreated')
  async postContractCreated(@Req() req): Promise<any> {
    return await this.subService.postContractCreated(req)
  }
  @UseGuards(JwtAuthGuard)
  @Post('hhsaveTalkReq')
  async postSaveTalkReq(@Req() req): Promise<any> {
    return await this.subService.postSaveTalkReq(req)
  }
  
  @UseGuards(JwtAuthGuard)
  @Post('hhCitizenPassed')
  async postPassed(@Req() req): Promise<any> {
    return await this.subService.postPassed(req)
  }
  
  @UseGuards(JwtAuthGuard)
  @Post('hhgotJob')
  async postgotJob(@Req() req): Promise<any> {
    return await this.subService.postgotJob(req)
  }
  @UseGuards(JwtAuthGuard)
  @Post('hhCitizenFailed')
  async postFailed(@Req() req): Promise<any> {
    return await this.subService.postFailed(req)
  }
  @UseGuards(JwtAuthGuard)
  @Post('hhCitizenPut')
  async putCitizen(@Req() req): Promise<any> {
    return await this.subService.putCitizen(req)
  }
  @UseGuards(JwtAuthGuard)
  @Post('hhRemoveEducation')
  async removeEducation(@Req() req): Promise<any> {
    return await this.subService.removeEducation(req)
  }
  @UseGuards(JwtAuthGuard)
  @Post('hhRemoveContact')
  async removeContact(@Req() req): Promise<any> {
    return await this.subService.removeContact(req)
  }
  @UseGuards(JwtAuthGuard)
  @Post('hhBeforeGoing')
  async removeBeforeGoing(@Req() req): Promise<any> {
    return await this.subService.removeBeforeGoing(req)
  }
  @UseGuards(JwtAuthGuard)
  @Post('hhremoveWork')
  async removeWork(@Req() req): Promise<any> {
    return await this.subService.removeWork(req)
  }
  @UseGuards(JwtAuthGuard)
  @Post('removeCetrificate')
  async removeCetrificate(@Req() req): Promise<any> {
    return await this.subService.removeCetrificate(req)
  }
  @UseGuards(JwtAuthGuard)
  @Post('removeSkillCheck')
  async removeSkillCheck(@Req() req): Promise<any> {
    return await this.subService.removeSkillCheck(req)
  }
  @UseGuards(JwtAuthGuard)
  @Post('removeWantPlace')
  async removeWantPlace(@Req() req): Promise<any> {
    return await this.subService.removeWantPlace(req)
  }
  // Байгууллага
  @Get('hhOrganizationGet')
  async getOrganization(@Req() req): Promise<any> {
    return await this.subService.getOrganizationList(req)
  }
  @Get('hhRefCategory')
  async getRefCategory(): Promise<any> {
    return await this.subService.getRefCategory()
  }
  @Get('hhWorkforceTypes')
  async gethhWorkforceTypes(): Promise<any> {
    return await this.subService.gethhWorkforceTypes()
  }
  @Get('contractTypes')
  async getcontractTypes(): Promise<any> {
    return await this.subService.getcontractTypes()
  }

  @UseGuards(JwtAuthGuard)
  @Post('hhOrganizationPost')
  async postOrganization(@Req() req): Promise<any> {
    return await this.subService.postOrganization(req)
  }

  @UseGuards(JwtAuthGuard)
  @Post('hhOrganizationPut')
  async putOrganization(@Req() req): Promise<any> {
    return await this.subService.putOrganization(req)
  }
}
