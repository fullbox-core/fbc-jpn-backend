module.exports = {
  apps : [{
    name   : "JPN-BACKEND",
    script : "dist/main.js",
    env: {
      NODE_ENV: "production"
    }
  }]
}